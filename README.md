# KADplus
Download your favorite Manga in a well-organized structure, without any extra effort and without navigating through pages. 
<br>**It stores the downloaded Mangas in saparate folders, each folder contains subfolders for each chapter.**

GUI version of [KADManga](https://github.com/mohabmes/KADManga).

**Note: KADplus is still under development.**